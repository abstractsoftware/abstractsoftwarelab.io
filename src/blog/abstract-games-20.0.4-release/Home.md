Abstract Games makes a minor release! However, soon it is expected that a new arcade game (Frogger), and a new board game (Checkers) will be released.

## New Dialogs for a More Modern Look

Now Abstract Games includes embedded dialogs, instead of the system dialogs it used before. One advantage of this will be that it will work better on mobile operating systems. Another is that they look far more modern. Thanks to Qt Quick Controls 2 API, they are designed with a material style look, and dark to contrast the rest of the game.

## Focusing on Arcade and Board Games

Now, we are focusing on creating board games, and classic arcade games. Why? Well, it was found out that our strategy games where not as popular as the rest of our games. Even though now, as video games become more popular, there still is a need for classical board games, which have remained popular. In fact, board games that have multiple players are called "abstract games", making it important to stay true to our name.

## Arcade Games Improved

Several bug fixes have been made to JetFighter (and Frogger, though it has not been released yet), making it easier to play and use. Among the bug fixed are how it annoyingly "froze" and the end. Also, now it is easy to see how many lives and points you have. While lots of games show this information in the title bar, we instead use a label in an appropriate location so that users can easily see it, however, still it does not disturb the game. Also, now if your cursor or window gets out of focus, it will automatically pause it for you, however, instead of using the standard pause screen, it uses one which is low on animations and easy to continue again.
