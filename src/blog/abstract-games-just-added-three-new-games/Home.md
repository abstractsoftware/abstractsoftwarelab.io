Abstract Games version 20.2.0 has just added three new games. They include one new board game, and our first arcade game, called JetFighter.

## New Games

The three new games added were called based on Go, Bomber, and Robots. To run, merely search in your launcher for iGo, JetFighter, and Bots. JetFighter is our first arcade game, which in fact has been released while it is still in beta, but nonetheless includes gravity, bombs, and a plane which the user can control. Go has an AI, which is only half complete at the moment, but is going to be made with a similar design to the Chess and TicTacToe AI.

## Improvements

The chess engine has been improved, but hasn't been made faster yet. Now it includes pawn evaluation, and material evaluation, which should made a better playing experience. TicTacToe's AI has been improved, so now TicTacToe (along with Mines) will stay stable for a long time now. Chess has been improved, so that the user can easily see what move has the computer played, so it should be easier to decide what move to make.

## Coming Next

Coming next will be frogger, another arcade game, which is being developed right now, and predicted to be ready in several weeks time. Also, now a unified framework for creating games, so anyone will be able to make a game, and request for it to be added to the game's suite. LibAbstractGames will include a framework for logic, game widgets, and the game loop, so that it should be easy for anyone to create games.
