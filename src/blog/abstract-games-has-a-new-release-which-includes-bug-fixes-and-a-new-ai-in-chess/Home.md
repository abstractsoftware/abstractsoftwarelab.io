The release 2020.1.1 has just come out for Abstract Games. It includes bug fixes, and a new AI in chess and other more.
## Bugs Fixed in Chess and SameGame
Many bugs were fixed in chess, including problems with the rules, and with finding out rather the user has won or not, and finding out rather you are Check or not. Users would of found these bugs very annoying, so this was an important update.
## A New Very Slow AI for Chess
A new AI has been made in Chess. Currently, it's very slow and not very smart, but if you have a fairly fast computer, then it can make some good moves. In the next release, a faster AI shall be made for a better usage. This AI uses the standard alpha-beta method that many chess engines use (see [Chess Programming Wiki](https://www.chessprogramming.org/Alpha-beta)).
## The Next Release: Robots
In the next few releases, a new game called Robots shall be releases. This game used to be for the OpenBSD command line, and KDE and gnome make versions of it for Linux already. The first version has already been made, but is cannot be used by the public yet. However, soon Robots will be released, perhaps in the next release.
