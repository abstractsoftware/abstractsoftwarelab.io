Now anyone can add a blog post to our website, because our website is completely open source and anyone can submit pull requests to it.

## How to Add Blog Posts

You can view the source code of this website at [gitlab.com](https://gitlab.com/abstractsoftware/abstractsoftware.gitlab.io). To make a new blog post, follow the instructions in the README (look at the section on "Adding Blog Posts"). It's simple, and it uses markdown, which is an easy to use markup language, which is also very easy to learn quickly. It also uses python3 for automating tasks, to make it easy to use. It merely involves running a small command line application, then using a built-in template to make your blog post, and see all of the supported markdown, and rules that you must use. You can include images, lists, and links into blog posts. 

## More About this Website

This website is open source, and doesn't use any static website generator, just markdown, yaml, json, html, javascript, and css. It is hosted on gitlab, the reason why it has a \*.gitlab.io extension, and uses [gitlab CI/CD](https://gitlab.com/help/ci/README.md) and python to convert the easy to use markdown into powerful html, css, and javascript. And everything is automated: new versions of the website will be released regularly without any trouble at all, and making new blog posts is very easy (see above). We are planning on making new blog posts once a month, but we accept pull requests any time.
