The O20 Feb2 release is here! Among the major updates to this release are updated and saving styles, editorial comments, and under the hood, a completely redesigned Spectrum.  
Among the features added in this release are the following:

* New rounded and detatched splash screen
* Access your recent files quickly from the Start Screen
* Put TODO's and comments in your document, then view them from the sidebar.

## New icons!
Becuase of worries about using the Microsoft trademarked icons ([Issue](https://gitlab.com/abstractsoftware/o20/o20coreapps/issues/4)), we have decided to use some entirely new icons:  
![image](https://gitlab.com/abstractsoftware/o20/o20coreapps/-/raw/master/dev/res/icons/rebrand/ms-office.svg?inline=false)
![image](https://gitlab.com/abstractsoftware/o20/o20coreapps/-/raw/master/dev/res/icons/rebrand/ms-word.svg?inline=false)
![image](https://gitlab.com/abstractsoftware/o20/o20coreapps/-/raw/master/dev/res/icons/rebrand/ms-onenote.svg?inline=false)
![image](https://gitlab.com/abstractsoftware/o20/o20coreapps/-/raw/master/dev/res/icons/rebrand/ms-powerpoint.svg?inline=false)  
The credit goes to the McMojave Circle icon theme, the icon set with the most downloads on KDE Plasma!

## OneNote is in the last stage of Development
We are happy to announce that the first version of OneNote will soon be ready after much development, and will hopefully be ready at Mar3 release!  

## O20 now has Flatpak
We now have a Flatpak for O20, and all users with the Snap are reccomended to migrate to the Flatpak as it works better. Find out more here:  
[O20.Word Flatpak page](https://flathub.org/apps/details/io.gitlab.o20.word)

## Spectrum remade
Now Spectrum comes with an entirely new interface since 20.1, including a chat based interface. A conversation with Spectrum might now go like this:  
    Welcome back to O20, adev!  
    Go to line 52  
    Done!  
    Select this word.  
    Done!  
    Italic and bold.  
    Done too!  
    Insert 'Spectrum is a great AI!!'  
    Done too!  
    Select this paragraph  
    Done too!  
    Align center.  
    Done.  
    Thanks, Spectrum!!  
    Don't mention it.  

Keep in mind that some of this hasn't been implemented yet, but with a redesigned code base for Spectrum, it will be simple to implement all of these things.
