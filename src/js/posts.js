var STATE_LOADED = 4, STATUS_OK = 200;

function Posts()
{
	Posts.prototype = {
		xhr: new XMLHttpRequest(),

		get: function ()
		{
			this.xhr.onreadystatechange = this.stateChange;
			this.xhr.open("GET", "/data/posts.json");
			this.xhr.send();
		},

		stateChange: function ()
		{
			if (this.status == STATUS_OK)
			{
				if (document.children[0].children[1].children[0].children[1].children.length != 1) return;

				console.log("parsing...");
				var data = JSON.parse(this.responseText);
				var nav = document.children[0].children[1].children[0].children[1];

				for (var i = 0; i < data["posts"].length; i++)
				{
					var spacer = document.createElement("div");

					spacer.style = "margin-top: 30px";
					nav.appendChild(spacer);

					var post = document.createElement("div");

					post.className = "post";

					var title = document.createElement("div");
					var summary = document.createElement("p");
					var name = document.createElement("div");
					var date = document.createElement("p");

					title.className = "title";
					summary.className = "summary";
					name.className = "name";
					date.className = "date";

					var link = document.createElement("a");

					link.className = "link";
					link.setAttribute("href", data["posts"][i]["href"]);
					link.innerHTML = data["posts"][i]["title"];

					title.appendChild(link);

					summary.innerHTML = data["posts"][i]["summary"];
					name.innerHTML = data["posts"][i]["author"];
					date.innerHTML = data["posts"][i]["date"];

					post.appendChild(title);
					post.appendChild(summary);
					post.appendChild(name);
					post.appendChild(date);
					nav.appendChild(post);
				}
			}
		},
	}
}

window.addEventListener("load", function () { new Posts(); (new Posts()).get(); });
