function onStyleChange(h, i)
                {
                        if (h)
                        {
                                i.style.textDecoration = "underline";
                        }
                        else
                        {
                                i.style.textDecoration = "none";
                        }
                }

function Header()
{
	Header.prototype = {
		header: document.createElement("header"),
		home: document.createElement("a"),
		blog: document.createElement("a"),
		download: document.createElement("a"),

		create: function ()
		{
			this.home.innerHTML = "Home"
			this.home.style.position = "relative";
			this.home.style.left = "20px";
			this.home.style.top = "10px";
			this.home.style.color = "black";
			this.home.style.borderRadius = "5px";
			this.home.style.padding = "3px";
                        this.home.style.textDecoration = "none";
			this.home.id = "home-item";

			this.home.setAttribute("title", "Go to Home");
			this.home.setAttribute("href", "javascript:void(0)");
			this.home.onclick = this.homeClicked;
			this.home.onpointerenter = function () { onStyleChange(true,  document.getElementById("home-item")); };  /*
					document.getElementById("home-item").style.backgroundColor = "#d98806";  }*/
			this.home.onpointerleave = function () { onStyleChange(false, document.getElementById("home-item"));  };
				/*document.getElementById("home-iem").style.backgroundColor = "transparent";  }*/

			this.blog.innerHTML = "Blog";
			this.blog.style.position = "relative";
			this.blog.style.left = "50px";
			this.blog.style.top = "10px";
			this.blog.style.color = "black";
			this.blog.style.borderRadius = "5px";
			this.blog.style.padding = "3px";
			this.blog.style.paddingLeft = "7px";
			this.blog.style.paddingRight = "7px";
			this.blog.style.textDecoration = "none";
			this.blog.id = "blog-item";

			this.blog.setAttribute("title", "Go to Blog");
                        this.blog.setAttribute("href", "javascript:void(0)");
                        this.blog.onclick = this.blogClicked;
			this.blog.onpointerenter = function () { onStyleChange(true,  document.getElementById("blog-item")); };  /*
                                        document.getElementById("home-item").style.backgroundColor = "#d98806";  }*/
                        this.blog.onpointerleave = function () { onStyleChange(false, document.getElementById("blog-item"));  };
                                /*document.getElementById("home-iem").style.backgroundColor = "transparent";  }*/

			this.header.style.height = "40px";

			this.download.innerHTML = "Downloads";
                        this.download.style.position = "relative";
                        this.download.style.left = "70px";
                        this.download.style.top = "10px";
                        this.download.style.color = "black";
                        this.download.style.borderRadius = "5px";
                        this.download.style.padding = "3px";
                        this.download.style.paddingLeft = "7px";
                        this.download.style.paddingRight = "7px";
                        this.download.style.textDecoration = "none";
                        this.download.id = "download-item";

                        this.download.setAttribute("title", "Go to Downloads");
                        this.download.setAttribute("href", "javascript:void(0)");
                        this.download.onclick = this.downloadClicked;
                        this.download.onpointerenter = function () { onStyleChange(true,  document.getElementById("download-item")); };  /*
                                        document.getElementById("home-item").style.backgroundColor = "#d98806";  }*/
                        this.download.onpointerleave = function () { onStyleChange(false, document.getElementById("download-item"));  };
                                /*document.getElementById("home-iem").style.backgroundColor = "transparent";  }*/

                        this.header.style.height = "40px";

			this.header.appendChild(this.home);
			this.header.appendChild(this.blog);
			this.header.appendChild(this.download);

			document.children[0].children[1].children[0].insertBefore(this.header, document.children[0].children[1].children[0].children[0]);
		},

		homeClicked: function ()
		{
			parent.location.href="/";
		},

		blogClicked: function ()
		{
			parent.location.href = "/blog";
		},

		downloadClicked: function ()
		{
			parent.location.href = "/downloads";
		}
	}
}

window.addEventListener("load", function () { new Header(); (new Header()).create(); });
