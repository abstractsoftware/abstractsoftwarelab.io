function SplashScreen()
{
	SplashScreen.prototype = {
		icon: document.createElement("img"),
		progessBar: document.createElement("div"),
		backing: document.createElement("div"),

		start: function ()
		{
			this.icon.setAttribute("src", "/favicon.ico");
			this.icon.setAttribute("width", "64");
			this.icon.setAttribute("height", "64");
			this.icon.style.position = "absolute";
			this.icon.style.top = "40%"
			this.icon.style.left = "50%";

			this.progessBar.style.width = "10px";
			this.progessBar.style.height = "2px";
			this.progessBar.style.borderRadius = "5px";
			this.progessBar.style.position = "absolute";
			this.progessBar.style.color = "white";
			this.progessBar.style.top = "60%";
			this.progessBar.style.left = "44%";
			this.progessBar.style.background = "white";
			this.progessBar.setAttribute("id", "progess");

			this.backing.style.position = "absolute";
			this.backing.style.left = "0";
			this.backing.style.right = "0";
			this.backing.style.top = "0";
			this.backing.style.bottom = "0";
			this.backing.style.backgroundColor = "#f9a826";

			this.backing.appendChild(this.icon);
			this.backing.appendChild(this.progessBar);

			document.children[0].children[1].children[0].hidden = true;
			document.children[0].children[1].appendChild(this.backing);

			setTimeout(this.finish, 500);

			for (var i = 50; i < 500; i += 10)
			{
				setTimeout(this.addProgess, i);
			}
		},

		addProgess: function ()
		{
			var width = parseFloat(window.getComputedStyle(document.getElementById("progess")).width.replace("px", ""));
			document.getElementById("progess").style.width = (width + 4).toString() + "px";
		},

		finish: function ()
		{
			document.children[0].children[1].children[0].hidden = false;
			document.children[0].children[1].children[1].hidden = true;
		}
	}
}

window.addEventListener("load", function () { new SplashScreen(); (new SplashScreen()).start(); });
