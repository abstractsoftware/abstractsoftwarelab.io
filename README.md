# abstractsoftware.gitlab.io

is the source code for The Abstract Software Project's website, which is hosted at [abstractsoftware.gitlab.io](https://abstractsoftware.gitlab.io). This is a static website that uses plain HTML, CSS, and Javascript markup without any static website generator. It uses python for some simple task, such as converting markdown into html, and running test build-sites.

## Testing Website

To test out this website on your local computer, first install the packages `python3` and `python3-markdown`. To build, run:

```sh
python3 build-site.py --source src --target .build --build
```

To start a server on your local computer, run:

```sh
python3 build-site.py --target .build --start-server
```

To stop the server, run:

```sh
python3 build-site.py --target .build --stop-server
```

Don't forget to build every time you make a change to the website. While the server is running, you can preview the website at `localhost:1313` All of the source is located in the sub directory `src`, and when you're finished, you can delete the directory `.build`, as that is no longer needed. The script that you run to test the website will convert all of the blog posts in html, and runs a local server on your computer.

## Adding Blog Posts

To make a new blog post, install the packages above, then fork this repository. After you fork, run the following commanding inside of the cloned fork:

```sh
python3 add-post.py -title "Blog Post Title" -summary "Blog Post Summary" -author "Author of Blog Post"
```

Replace `Blog Post Title`, `Blog Post Summary`, and `Author of Blog Post` with the title, summary, and author of your blog post. Afterwards, look in the directory `src > blog > blog-post-title` (replace `blog-post-title` with the name of your blog post in lowercase with spaces replaced by dashes), and you should see a file called `Home.md`. Open it with your text editor, and it should contain a template of a blog post. Read this template carefully, then delete all the text and replace it with your blog post. This file is a markdown file, which is a easy to learn and use markup language. Afterwards, remember to add the blog post and any images, and then submit a pull request to us.
