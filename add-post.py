'This script will add a blog post.'

import json
import sys
import os

from os.path import *
from datetime import *

def cleardir(dir2clear):
    for root, dirs, files in os.walk(dir2clear):
        for f in files:
            os.remove(join(root, f))

args = iter(sys.argv[1:])
num = 0

for arg in args:
    if arg == '-title':
        title = next(args).strip()
        num += 1
    elif arg == '-summary':
        summary = next(args).strip()
        num += 1
    elif arg == '-author':
        author = next(args).strip()
        num += 1

if num == 0:
    print('Usage: python3 add-post.py -title title -summary summary -author author')
    raise SystemExit

date = datetime.now().date().strftime('%B %d, %Y')
href = title.lower().replace(' ', '-')
href = href.replace(',', '')

try: os.mkdir(join('src', join('blog', href)))
except: cleardir(join('src', join('blog', href)))

f = open('src/blog/%s/Home.md' % href, 'w')
f.write('Please read this document carefully before you continue\n')
f.write('## Rules\n')
f.write('Always use heading two (##), not heading one (#). Always use the supported markdown:\n')
f.write('* list item\n')
f.write('[link](https://abstractsoftware.gitlab.io)\n')
f.write('![image](https://abstractsoftware.gitlab.io/favicon.ico)\n')
f.write('*italic*')
f.write('**strong**')
f.write('Put images either in the same directory as blog post, or on a different server. Make sure they are not two large. If they are, scale them down. Do not include author name or title of blog post at the top of the blog post, and please do not put signature at the end of blog post.\n')
f.write('Please make the first paragraph just a few sentences long, perhaps just a short summary of your blog post.\n')
f.write('## Deploying\n')
f.write('Now you are all set! Make sure to add the current directory you are in, then submit a pull request to us.\n')

f = open('src/data/posts.json', 'r')
data = json.load(f)

data['posts'] = [{'title': title, 'href': href, 'summary': summary, 'author': author, 'date': date}] + data['posts']

f = open('src/data/posts.json', 'w')
f.write(json.dumps(data, indent=4))
