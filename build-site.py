'Builds the site and runs a test server'

from http.server import *
from os.path import *
import os, sys
import json
import markdown

def cleardir(dir2clear):
    for root, dirs, files in os.walk(dir2clear):
        for f in files:
            os.remove(join(root, f))

def removedir(dir2remove):
    cleardir(dir2remove)

    for root, dirs, files in os.walk(dir2remove):
        for f in dirs:
            try: os.removedirs(join(root, f))
            except: pass

args = iter(sys.argv[1:])
num = 0

for arg in args:
   if arg == '--source':
       source = next(args)
   elif arg == '--target':
       target = next(args)
   elif arg == '--start-server':
       try:
           os.chdir(target)
       except FileNotFoundError:
           print('Error: target not found')
           sys.exit(-1)

       if os.access('.pid', os.F_OK):
           print('Error: server found')
           sys.exit(-1)

       pid = os.spawnv(os.P_NOWAIT, '/usr/bin/python3', ('python3', 
           '-cfrom http.server import*;import sys;sys.stderr=open(".log", "w");HTTPServer(("localhost",1313),SimpleHTTPRequestHandler).serve_forever()'))

       f = open('.pid', 'w')
       f.write(str(pid))

       num += 1
   elif arg == '--stop-server':
       try:
           os.chdir(target)
       except FileNotFoundError:
           print('Error: target not found')
           sys.exit(-1)

       f = open('.pid', 'r')
       pid = int(f.read())

       try:
           os.kill(pid, 9)
       except ProcessLookupError:
           print('Error: server not found')
           sys.exit(-1)

       os.remove('.pid')

       num += 1
   elif arg == '--build':
       # Make directory, or clear it if exists

       try: os.mkdir(target)
       except: cleardir(target)

       os.chdir(source)

       # Copy files into directory

       for root, dirs, files in os.walk(os.curdir):
           if root.find(target) != -1: continue

           try: os.mkdir(join(join(os.pardir, target), root))
           except: pass

           for f in files:
                f1 = open(join(root, f), 'rb')
                f2 = open(join(join(os.pardir, target), join(root, f)), 'wb')
                f2.write(f1.read())

       os.chdir(os.pardir)

       # Read JSON

       f = open(join(source, join('data', 'posts.json')), 'r')
       data = json.load(f)

       for d in data['posts']: 
            href = d['href']
            title = d['title']
            summary = d['summary']
            date = d['date']
            author = d['author']

            #for root, dirs, files in os.walk('%s/blog/%s' % (source, href)):
            #    for f in files#:
            #        f1 = open(join(root, f), 'rb')
            #        f2 = open('%s/blog/%s/%s' % (target, href, f), 'wb')
            #        f2.write(f1.read())
            #        f1.close()
            #        f2.close()

            f = open('%s/blog/%s/Home.md' % (source, href), 'r')

            text = f.read()

            html = '<html>'
            html += '<head>'
            html += '<title>%s</title>' % title
            html += '<meta author="keywords" content="The Abstract Software Project, Blog, Office 20, Abstract Games, Office 365 for Linux, Modern Games for a New Decade,' 
            html += 'The Abstract Developers, %s"/>' % title
            html += '<meta author="description" content="%s"/>' % title
            html += '<link rel="stylesheet" href="/css/style.css"/>'
            html += '<script src="/js/header.js"></script>'
            html += '<script src="/js/splashscreen.js"></script>'
            html += '</head>'
            html += '<body>'
            html += '<main hidden="true">'
            html += '<article>'
            html += '<h1>%s</h1>' % title
            html += author
            html += '<p class="date">%s</p>' % date
            html += '<div style="margin-top: 30px"></div>'

            html += markdown.markdown(text)

            html += '</article>'
            html += '</main>'
            html += '</body>'
            html += '</html>'

            try: os.mkdir(join(target, join('blog', href)))
            except: cleardir(join(target, join('blog', href)))

            f = open(join(target, join('blog', join(href, 'index.html'))), 'w')
            f.write(html)

            for root, dirs, files in os.walk('%s/blog/%s' % (source, href)):
                for f in files:
                    f1 = open(join(root, f), 'rb')
                    f2 = open('%s/blog/%s/%s' % (target, href, f), 'wb')
                    f2.write(f1.read())
                    f1.close()
                    f2.close()

       num += 1

if num == 0:
    print('Usage:')
    print('python3 build-site.py --source source --target target --build')
    print('python3 build-site.py --target target --start-server|--stop-server')
    raise SystemExit
